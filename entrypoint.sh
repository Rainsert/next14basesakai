#!/bin/sh
# entrypoint.sh

# Crear el archivo .env.production con las variables de entorno
echo "NEXTAUTH_URL=${NEXTAUTH_URL}" > /app/.env.production
echo "NEXTAUTH_SECRET=${NEXTAUTH_SECRET}" >> /app/.env.production
echo "JWT_SECRET=${JWT_SECRET}" >> /app/.env.production
echo "NEXT_PUBLIC_BASE_PATH=${NEXT_PUBLIC_BASE_PATH}" >> /app/.env.production

# Ejecutar el comando pasado al entrypoint (CMD en Dockerfile)
exec "$@"
