"use client"

import { signOut } from 'next-auth/react'
import React from 'react'

const BtnLogOut = () => {
  return (
    <button type="button" onClick={() => signOut()} className="p-link layout-topbar-button h-min p-2 m-0">
        <i className="pi pi-sign-out"></i>
        <span>Cerrar sesión</span>
    </button>
  )
}

export default BtnLogOut
